/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdio.h>
#include <string.h>
#include "project.h"
#include "debug.h"
#include "custom_lib.h"

int main(void)
{
    float second = 0;
    char *buf = malloc(2 * sizeof(float) + 1); // add one more byte for line termination
    
    CyGlobalIntEnable; /* Enable global interrupts. */
    Logger_Init();
    
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    DBG_NEW_PAGE();
    DBG_PRINT_TEXT("-------------------------------------------------- Application Start --------------------------------------------------\r\n\r\n");
    LOG_ERROR("Main Function", "Print error to terminal.");
    LOG_WARN("Main Function", "Print warning to terminal.");
    LOG_DEBUG("Main Function", "Print debug to terminal.");
    LOG_INFO("Main Function", "Print info to terminal.");
    LOG_ABSOLUTE("Main Function", "Print message to terminal without any log type.");
    CyDelay(1000);
    
    for(;;)
    {
        /* Place your application code here. */
        if (second < 10.0)
        {
            second = (float)millis() / 1000.0;
            LOG_INFO("Timer Update", "Get timer value from Logger: %.3f s", second);
            Hex_To_Ascii(buf, (uint8 *)&second, sizeof(float));    
            CyDelay(1000);
        }
        free(buf);
    }
}

/* [] END OF FILE */
