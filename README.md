# PSoC Logger

Logger library modified from PSoC debug library running at a maximum of 1Mbps baudrate and display more useful information for PSoC ARM processor.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Figure below shows PSoC schematic and the expected result from the library to be displayed on PuTTY terminal.

![top_design](Images/top_design.png)



![logger_output](Images/logger_output.png)

### Prerequisites

* Software: 
  * PSoC Creator
  * PuTTY or any serial monitor equivalent software that is capable of displaying ANSI escape sequence
* Hardware:
  * PSoC 4, PSoC 4 BLE or PSoC 5LP development kit

### Usage

To enable the logger, make sure that macro `LOG_ENABLED` is set in the compiler preprocessor definition as shown below. This make it easier to turn on or off logger depend on different project configurations.

![enable_logger](Images/enable_logger.png)



Additionally, you can also enable Runtime of the device by enabling the timer in `config.h`. This will start a 1 ms timer and display it alongside the logger.  The timer is also useful even if you disable the logger and have the timer running, in order to calculate debounce time for button, or calculate runtime of a function, etc.

```c
#ifndef MS_TIMER_ENABLED
#define MS_TIMER_ENABLED 1
#endif
```

## Running the tests

Simply select the target device for the project, build and program the target. To display the log, open PuTTY or equivalent terminal and select the right COM port and matching baudrate as the ones configured in PSoC Creator.

## Authors

* **Sovichea Tep**	<sovichea.tep@gmail.com>

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details