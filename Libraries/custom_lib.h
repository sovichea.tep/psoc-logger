#ifndef CUSTOM_LIB_H_
#define CUSTOM_LIB_H_

#include "project.h"
#include "debug.h"

void Hex_To_Ascii(char *buf, uint8 *bytes, uint8 size);
    
#endif /* CUSTOM_LIB_H_ */