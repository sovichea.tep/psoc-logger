#include "custom_lib.h"

void Hex_To_Ascii(char *buf, uint8 *bytes, uint8 size)
{
    uint8 i;
    char *ptr = buf;
    for(i = 0; i < size; ++i)
    {
        uint8 high = bytes[i] >> 4;
        uint8 low = bytes[i] & 0x0F;
        *ptr = high + '0' + (7 * (high / 10));
        *(ptr + 1) = low + '0' + (7 * (low / 10));
        ptr += 2;
    }
    // add line termination to the end of the string
    *ptr = 0;
    LOG_DEBUG("Hex_To_Ascii", buf);
}