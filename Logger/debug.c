/*******************************************************************************
* File Name: debug.c
*
* Version: 1.20
*
* Description:
*
* Hardware Dependency:
*  CY8CKIT-042 BLE
*
********************************************************************************
* Copyright 2014-2016, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#include "cytypes.h"
#include "debug.h"
#include "project.h"

#ifdef LOG_ENABLED
    
void __log_function(log_type_t log_type, const char *filename, uint32 line, const char *tag, const char *message, ...) 
{
    char buffer[64];
    char temp[64];
    va_list args;
    va_start(args, message);
    
    /* Example of log format
    
    [ s ] [log type]     [file:line]   [log tag]             [log message]
    0.001 [ERROR]         main.c:29     Main Function      >> Print error to terminal.
    
    */
    
    /* print runtime */
    snprintf(buffer, sizeof(buffer), "%8.3f", millis() / 1000.0);
    DBG_PRINT_TEXT(buffer);
    
    /* print log mode */
    switch(log_type)
    {
        case LOG_ERROR:
            snprintf(buffer, sizeof(buffer), " [%s%-5s%s]", BR, "ERROR", RESET);
            break;
        case LOG_WARN:
            snprintf(buffer, sizeof(buffer), " [%s%-5s%s]", BY, "WARN", RESET);
            break;
        case LOG_DEBUG:
            snprintf(buffer, sizeof(buffer), " [%s%-5s%s]", BG, "DEBUG", RESET);
            break;
        case LOG_INFO:
            snprintf(buffer, sizeof(buffer), " [%s%-5s%s]", BC, "INFO", RESET);
            break;
        default:
            snprintf(buffer, sizeof(buffer), " [%s%-5s%s]", BW, "-----", RESET);
            break;
    }
    DBG_PRINT_TEXT(buffer);
    
    /* print file name and line */
    snprintf(temp, sizeof(temp), "%s:%-4lu", filename, line);
    snprintf(buffer, sizeof(buffer), "\t%s%-20s%s", DS, temp, RESET);
    DBG_PRINT_TEXT(buffer);
    
    /* print log tag */
    snprintf(buffer, sizeof(buffer), "\t%s%-20s%s", DP, tag, RESET);
    DBG_PRINT_TEXT(buffer);
    
    /* print log message */
    vsnprintf(buffer, sizeof(buffer), message, args);
    DBG_PRINT_TEXT(DY "\t>> " RESET);
    DBG_PRINT_TEXT(buffer);
    DBG_PRINT_TEXT("\r\n");
}

#endif /* (LOG_ENABLED) */

void Logger_Init()
{
    /* initialize log timer */
    #if (MS_TIMER_ENABLED)
    Timer_Init();
    #endif
    
    /* initialize log uart */
    #ifdef LOG_ENABLED
    DEBUG_UART_Start();
    #endif
}

/* [] END OF FILE */
