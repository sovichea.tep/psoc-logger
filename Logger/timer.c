#include "timer.h"

#if (MS_TIMER_ENABLED == 1)
    
volatile uint32 counter_ms = 0;

CY_ISR(Get_Ms)
{
    counter_ms += 1;
    Timer_Ms_ClearInterrupt(Timer_Ms_INTR_MASK_TC);
}

void Timer_Init()
{
    Timer_Ms_Start();
    Isr_Timer_StartEx(Get_Ms);
}

uint32 millis()
{
    return counter_ms;
}

#else /* (MS_TIMER_ENABLED == 1) */
    
void Timer_Init() {return;}
uint32 millis() {return 0;}

#endif /* (MS_TIMER_ENABLED == 0) */