/*******************************************************************************
* File Name: debug.h
*
* Version: 1.20
*
* Description:
*  Provides debug API.
*
* Hardware Dependency:
*  CY8CKIT-042 BLE
*
********************************************************************************
* Copyright 2014-2016, Cypress Semiconductor Corporation. All rights reserved.
* This software is owned by Cypress Semiconductor Corporation and is protected
* by and subject to worldwide patent and copyright laws and treaties.
* Therefore, you may use this software only as provided in the license agreement
* accompanying the software package from which you obtained this software.
* CYPRESS AND ITS SUPPLIERS MAKE NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
* WITH REGARD TO THIS SOFTWARE, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT,
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*******************************************************************************/

#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "cytypes.h"
#include "project.h"
#include "config.h"
#include "timer.h"

#ifdef LOG_ENABLED
    #define __FILENAME__ strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__
   
    #define ESC     "\033"
    #define RESET   ESC "[0m"
    
    // Dark colors defines.
    #define DB    ESC "[30m"    // Dark Black.
    #define DR    ESC "[31m"    // Dark Red.
    #define DG    ESC "[32m"    // Dark Green.
    #define DY    ESC "[33m"    // Dark Yellow.
    #define DS    ESC "[34m"    // Dark Sky. (Dark blue)
    #define DP    ESC "[35m"    // Dark Pink.
    #define DC    ESC "[36m"    // Dark Cyan.
    #define DW    ESC "[37m"    // Dark white. (Light Gray)

    // Bold light colors defines.
    #define BB    ESC "[30;1m"  // Light Black.
    #define BR    ESC "[31;1m"  // Light Red.
    #define BG    ESC "[32;1m"  // Light Green.
    #define BY    ESC "[33;1m"  // Light Yellow.
    #define BS    ESC "[34;1m"  // Light Sky. (Light blue)
    #define BP    ESC "[35;1m"  // Light Pink.
    #define BC    ESC "[36;1m"  // Light Cyan.
    #define BW    ESC "[37;1m"  // Light white.

    #define DD    ESC "[39m"    // Default color.
    
    #define DBG_PRINT_TEXT(a)   (DEBUG_UART_UartPutString(a))
    #define DBG_NEW_PAGE()      (DEBUG_UART_UartPutChar('\x0C'))
    
    typedef enum {
        LOG_ERROR,
        LOG_WARN,
        LOG_DEBUG,
        LOG_INFO,
        LOG_ABSOLUTE
    } log_type_t;
    
    #define LOG_ERROR(...)          (__log_function(LOG_ERROR   , __FILENAME__, __LINE__, __VA_ARGS__))
    #define LOG_WARN(...)           (__log_function(LOG_WARN    , __FILENAME__, __LINE__, __VA_ARGS__))
    #define LOG_DEBUG(...)          (__log_function(LOG_DEBUG   , __FILENAME__, __LINE__, __VA_ARGS__))
    #define LOG_INFO(...)           (__log_function(LOG_INFO    , __FILENAME__, __LINE__, __VA_ARGS__))
    #define LOG_ABSOLUTE(...)       (__log_function(LOG_ABSOLUTE, __FILENAME__, __LINE__, __VA_ARGS__))

    void __log_function(log_type_t log_type, const char *filename, uint32 line, const char *tag, const char *message, ...);

#else
    #define DBG_PRINT_TEXT(a)           do { (void)0; } while (0)
    #define DBG_NEW_PAGE()              do { (void)0; } while (0)
    
    #define LOG_ERROR(...)              do { (void)0; } while (0)
    #define LOG_WARN(...)               do { (void)0; } while (0)
    #define LOG_DEBUG(...)              do { (void)0; } while (0)
    #define LOG_INFO(...)               do { (void)0; } while (0)
    #define LOG_ABSOLUTE(...)           do { (void)0; } while (0)
#endif /* (LOG_ENABLED) */

void Logger_Init();

#endif /* DEBUG_H_ */

/* [] END OF FILE */
