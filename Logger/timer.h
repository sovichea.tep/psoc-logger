#ifndef TIMER_H_
#define TIMER_H_
    
#include <stdio.h>
#include "project.h"
#include "config.h"
    
void Timer_Init();
uint32 millis();
    
#endif /* TIMER_H_ */